package kz.daurenidrissov.converter.ui

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_converter.*
import kz.daurenidrissov.common.base.BaseFragment
import kz.daurenidrissov.converter.presenter.ConverterPresenter
import kz.daurenidrissov.converter.R
import kz.daurenidrissov.converter.contract.ConverterContract
import kz.daurenidrissov.converter.model.CurrencyItem
import org.koin.androidx.viewmodel.ext.android.viewModel

class ConverterFragment : BaseFragment<ConverterContract.View, ConverterContract.Presenter>(),
    ConverterContract.View {

    companion object {
        fun create() = ConverterFragment()
    }

    private var mainHandler: Handler = Handler(Looper.getMainLooper())

    private val presenterImpl: ConverterPresenter by viewModel()
    override val presenter: ConverterContract.Presenter
        get() = presenterImpl

    private val converterAdapter = ConverterAdapter(
        clickListener = {
            currencyClick(it)
        }
    )

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_converter, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter.loadDataFromApi()
        presenter.getDataFromSP()
        presenter.exchangeCurrency(baseValEditText.text.toString())

        mainHandler.post(object : Runnable {
            override fun run() {
                presenter.loadDataFromApi()
                presenter.update()
                mainHandler.postDelayed(this, 1000)
            }
        })

        baseValEditText.addTextChangedListener(object: TextWatcher{
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                presenter.exchangeCurrency(s.toString())
            }
        })

        val converterManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        converterRecyclerView.apply {
            layoutManager = converterManager
            adapter = converterAdapter
        }
    }

    override fun showCurrencyList(list: List<CurrencyItem>) {
        converterAdapter.addItems(list)
        context?.let { converterAdapter.passContext(context = it) }
    }

    override fun showData(d: Double) {
        titleTextView.text = d.toString()
    }

    override fun currencyClick(position: Int) {
        presenter.changeBaseCurrency(position)
    }

    override fun showBaseCurrency(baseCurrency: CurrencyItem) {
        baseTextView.text = baseCurrency.name
        baseValEditText.text = SpannableStringBuilder(baseCurrency.value.toString())
        val imageName = baseCurrency.name.toLowerCase().trim()
        val resID = resources.getIdentifier(imageName, "drawable", context!!.packageName)
        val stringID = resources.getIdentifier(imageName, "string", context!!.packageName)
        baseImageView.setImageResource(resID)
        baseDetailsTextView.text = getString(stringID)
    }

    override fun updateValues(list: List<CurrencyItem>) {
        converterAdapter.updateVals(list)
        context?.let { converterAdapter.passContext(context = it) }
    }
}