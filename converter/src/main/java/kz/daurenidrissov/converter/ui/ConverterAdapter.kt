package kz.daurenidrissov.converter.ui

import android.content.Context
import android.net.Uri
import android.text.SpannableStringBuilder
import android.view.ContextMenu
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_converter.view.*
import kz.daurenidrissov.converter.R
import kz.daurenidrissov.converter.model.CurrencyItem
import java.io.FileNotFoundException

class ConverterAdapter(
    private val clickListener: (position: Int) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val  currencies = mutableListOf<CurrencyItem>()
    private var thisContext: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return CurrencyViewHolder(inflater, parent)
    }

    override fun getItemCount(): Int = currencies.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as CurrencyViewHolder).bind(currencies[position], clickListener)
    }

    fun passContext(context: Context){
        thisContext = context
    }

    fun addItems(list: List<CurrencyItem>) {
        currencies.clear()
        currencies.addAll(list)
        notifyDataSetChanged()
    }

    fun updateVals(list: List<CurrencyItem>) {
        currencies.forEachIndexed { index, currencyItem ->
            currencyItem.value = list[index].value
        }
        notifyDataSetChanged()
    }

    private class CurrencyViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.item_converter, parent, false)) {
        private val currencyTextView: TextView = itemView.currencyTextView
        private val valueEditText: EditText = itemView.valueEditText
        private val currencyImageView: ImageView = itemView.currencyImageView
        private val detailedTextView: TextView = itemView.detailedTextView
        private val newContext = itemView.context

        fun bind(currency: CurrencyItem, clickListener: (position: Int) -> Unit) {
            currencyTextView.text = currency.name
            valueEditText.text = SpannableStringBuilder(currency.value.toString())

            val imageName = currency.name.toLowerCase().trim()
            val resID = newContext.resources.getIdentifier(imageName, "drawable", newContext!!.packageName)
            currencyImageView.setImageResource(resID)
            val stringID = newContext.resources.getIdentifier(imageName, "string", newContext!!.packageName)
            detailedTextView.text = newContext.getString(stringID)

            itemView.setOnClickListener {
                clickListener(adapterPosition)
            }
        }
    }
}