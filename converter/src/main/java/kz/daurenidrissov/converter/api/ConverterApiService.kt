package kz.daurenidrissov.converter.api

import io.reactivex.Single
import kz.daurenidrissov.converter.model.CurrencyData
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

private const val BASE_URL = "https://hiring.revolut.codes/api/android/"
private const val VALUE = "latest?base=EUR"

interface ConverterApiService {
    companion object Factory {
        fun create(): ConverterApiService {
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()

            return retrofit.create(ConverterApiService::class.java)
        }
    }

    @GET(VALUE)
    fun get(): Single<CurrencyData>
}