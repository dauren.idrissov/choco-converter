package kz.daurenidrissov.converter.api

import android.content.SharedPreferences
import com.google.gson.GsonBuilder
import io.reactivex.Single
import kz.daurenidrissov.converter.model.CurrencyData

private const val STATS_PREF_KEY = "CURRENCY_PREF_KEY"

class SharedPrefApi(private val prefs: SharedPreferences) {

    fun saveData(data: CurrencyData) {
        val editor = prefs.edit()
        val jsonString = GsonBuilder()
            .create()
            .toJson(data)
        editor?.putString(STATS_PREF_KEY, jsonString)
        editor?.apply()
    }

    fun getData(): Single<CurrencyData> =
        Single.fromCallable{
            GsonBuilder()
                .create()
                .fromJson(
                    prefs.getString(STATS_PREF_KEY, ""),
                    CurrencyData::class.java
                )
        }
}