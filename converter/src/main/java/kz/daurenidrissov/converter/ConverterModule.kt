package kz.daurenidrissov.converter

import android.preference.PreferenceManager
import kz.daurenidrissov.common.base.InjectionModule
import kz.daurenidrissov.converter.api.ConverterApiService
import kz.daurenidrissov.converter.api.SharedPrefApi
import kz.daurenidrissov.converter.interactor.ConverterInteractor
import kz.daurenidrissov.converter.presenter.ConverterPresenter
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

object ConverterModule : InjectionModule {
    override fun create() = module {
        viewModel  { ConverterPresenter(get()) }
        single { ConverterApiService.create() }
        single { ConverterInteractor(get(), get()) }
        single { SharedPrefApi(PreferenceManager.getDefaultSharedPreferences(get())) }
    }
}
