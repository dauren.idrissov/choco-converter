package kz.daurenidrissov.converter.presenter

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kz.daurenidrissov.common.base.BasePresenter
import kz.daurenidrissov.converter.contract.ConverterContract
import kz.daurenidrissov.converter.interactor.ConverterInteractor
import kz.daurenidrissov.converter.model.CurrencyData
import kz.daurenidrissov.converter.model.CurrencyItem
import timber.log.Timber
import kotlin.math.roundToInt

class ConverterPresenter (private val converterInteractor: ConverterInteractor) :
    BasePresenter<ConverterContract.View>(),
    ConverterContract.Presenter{

    private var currencyItems = mutableListOf<CurrencyItem>()
    private var baseCurrency: CurrencyItem = CurrencyItem("null", 0.0)
    private var exchangedItems = mutableListOf<CurrencyItem>()
    private var enteredNumber = 0.0

    override fun loadDataFromApi() {
        converterInteractor.loadData()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    converterInteractor.saveData(result)

                },
                {
                    Timber.e(it, "Error to load data")
                }
            )
            .disposeOnCleared()
    }

    override fun getDataFromSP() {
        converterInteractor.getData()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    baseCurrency = CurrencyItem(result.baseCurrency, 1.0)
                    view?.showBaseCurrency(baseCurrency)
                    currencyItems.clear()
                    result.rates.forEach{
                        currencyItems.add(CurrencyItem(it.key, it.value))
                    }
                    currencyItems.sortBy { it.name }
                    currencyItems?.let { view?.showCurrencyList(it) }
                },
                {
                    Timber.e(it, "Error to get from SP")
                }
            )
            .disposeOnCleared()
    }

    override fun update() {
        converterInteractor.getData()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    currencyItems.forEach {
                        if(it.name.trim() != "EUR") {
                            it.value = result.rates.getValue(it.name)
                        }
                    }
                    if(baseCurrency.name.trim() != "EUR") {
                        baseCurrency.value = result.rates.getValue(baseCurrency.name)
                    }

                    exchangedItems.clear()
                    currencyItems.forEach {
                        exchangedItems.add(CurrencyItem(it.name, Math.round(enteredNumber/baseCurrency.value*it.value * 100.0) / 100.0))
                    }
                    exchangedItems.sortBy { it.name }
                    exchangedItems?.let { view?.updateValues(it) }
                },
                {
                    Timber.e(it, "Error to get from SP")
                }
            )
            .disposeOnCleared()
    }

    override fun changeBaseCurrency(position: Int) {
        if(exchangedItems.isEmpty()) {
            currencyItems.forEach {
                exchangedItems.add(it)
            }
        }
        exchangedItems.add(CurrencyItem(baseCurrency.name, enteredNumber))
        currencyItems.add(baseCurrency)
        baseCurrency = currencyItems[position]
        view?.showBaseCurrency(CurrencyItem(baseCurrency.name, exchangedItems[position].value))
        currencyItems.removeAt(position)
        exchangedItems.removeAt(position)
        exchangedItems.sortBy { it.name }
        currencyItems.sortBy { it.name }
        exchangedItems?.let { view?.showCurrencyList(it) }

    }

    override fun exchangeCurrency(s: String) {
        if(s == "" || s == ".") {
            enteredNumber = 0.0
        } else {
            enteredNumber = s.toDouble()
        }
        exchangedItems.clear()
        currencyItems.forEach {
            exchangedItems.add(CurrencyItem(it.name, Math.round(enteredNumber/baseCurrency.value*it.value * 1000.0) / 1000.0))
        }
        exchangedItems?.let { view?.showCurrencyList(it) }
    }
}