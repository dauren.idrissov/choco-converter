package kz.daurenidrissov.converter.interactor

import io.reactivex.Single
import kz.daurenidrissov.converter.api.ConverterApiService
import kz.daurenidrissov.converter.api.SharedPrefApi
import kz.daurenidrissov.converter.model.CurrencyData

class ConverterInteractor(
    private val apiService: ConverterApiService,
    private val sharedPref: SharedPrefApi
) {
    fun loadData(): Single<CurrencyData> = apiService.get()
    fun getData(): Single<CurrencyData> = sharedPref.getData()
    fun saveData(data: CurrencyData) { sharedPref.saveData(data) }
}