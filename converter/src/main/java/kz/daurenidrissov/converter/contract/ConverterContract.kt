package kz.daurenidrissov.converter.contract

import kz.daurenidrissov.common.base.MvpPresenter
import kz.daurenidrissov.common.base.MvpView
import kz.daurenidrissov.converter.model.CurrencyData
import kz.daurenidrissov.converter.model.CurrencyItem

interface ConverterContract {

    interface View : MvpView {
        fun showData(d: Double)
        fun showCurrencyList(list: List<CurrencyItem>)
        fun currencyClick(position: Int)
        fun showBaseCurrency(baseCurrency: CurrencyItem)
        fun updateValues(list: List<CurrencyItem>)
    }

    interface Presenter : MvpPresenter<View> {
        fun loadDataFromApi()
        fun getDataFromSP()
        fun changeBaseCurrency(position: Int)
        fun exchangeCurrency(s: String)
        fun update()
    }
}