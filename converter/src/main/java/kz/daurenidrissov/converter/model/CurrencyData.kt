package kz.daurenidrissov.converter.model

data class CurrencyData(
    val baseCurrency: String,
    val rates: MutableMap<String, Double>
)

data class CurrencyItem(
    val name: String,
    var value: Double
)