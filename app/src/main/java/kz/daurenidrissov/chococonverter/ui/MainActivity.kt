package kz.daurenidrissov.chococonverter.ui

import android.os.Bundle
import kz.daurenidrissov.chococonverter.R
import kz.daurenidrissov.chococonverter.KoinModules
import kz.daurenidrissov.common.base.AppMainContract
import kz.daurenidrissov.common.base.BaseActivity
import kz.daurenidrissov.converter.ui.ConverterFragment
import org.koin.android.BuildConfig
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.context.startKoin

class MainActivity : BaseActivity(), MainActivityContract.View, AppMainContract {

    private val presenter: MainActivityPresenter by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startKoin {
            androidContext(applicationContext)
            if (BuildConfig.DEBUG) printLogger()
            modules(KoinModules.modules)
        }

        showConverter()
    }

    override fun show() {
        
    }

    override fun showConverter() {
        replaceFragment(ConverterFragment.create())
    }
}


