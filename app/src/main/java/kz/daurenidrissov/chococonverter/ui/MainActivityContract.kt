package kz.daurenidrissov.chococonverter.ui

import kz.daurenidrissov.common.base.MvpPresenter
import kz.daurenidrissov.common.base.MvpView

interface MainActivityContract {
    interface View : MvpView {
        fun show()
    }

    interface Presenter : MvpPresenter<View> {
        fun load()
    }
}