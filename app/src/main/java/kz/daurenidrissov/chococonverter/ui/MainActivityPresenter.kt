package kz.daurenidrissov.chococonverter.ui

import kz.daurenidrissov.common.base.BasePresenter


class MainActivityPresenter: BasePresenter<MainActivityContract.View>(), MainActivityContract.Presenter {
    override fun load() {
        view?.show()
    }

}