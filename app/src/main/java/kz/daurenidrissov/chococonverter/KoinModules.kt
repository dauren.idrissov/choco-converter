package kz.daurenidrissov.chococonverter

import kz.daurenidrissov.converter.ConverterModule
import org.koin.core.module.Module

object KoinModules {
    val modules: List<Module> =
        listOf(
            ConverterModule.create()
        )
}
